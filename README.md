# Dockerized Nextcloud

The docker compose file is taken from here: https://hub.docker.com/_/nextcloud/

## Getting Started

```
docker-compose up
```

Visit `http://localhost:8080/`
